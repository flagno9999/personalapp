import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Usuario } from './interfaces/usuario';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, AfterViewInit {
  lista_usuarios: Usuario[]

  constructor() {  
    this.lista_usuarios = []
  }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
  
  }
 

  recibirUsuario(usuario:Usuario){
    console.log("recibirUsuario()")
    this.lista_usuarios.push(usuario) 

  }

}
